package net.tncy.calvet6u;

import net.tncy.calvet6u.Book;

public class BookService {
    public Book addBook(Book b){
        return new Book();
    };

    public boolean removeBook(Book b){
        return true;
    }

    public Book updateBook(Book b){
        return new Book();
    }

    public Book getBookById(String bookid){
        return new Book();
    }

}
